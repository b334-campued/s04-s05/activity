package com.zuitt.example;

public class Contact {
    private String name, contactNumber, address;

    public Contact(){};


    public Contact(String name, String contactNumber, String address) {
        this.name = name;
        this.contactNumber = contactNumber;
        this.address = address;
    }

    public String getName() {
        return this.name;
    }
    public String getContactNumber() {
        return this.contactNumber;
    }
    public String getAddress() {
        return this.address;
    }

    public void setContactNumber(String contactNumber) {
        //Concatenate
        this.contactNumber += "\n" + contactNumber;
    }

    public void setAddress(String address) {
        //Concatenate
        this.address += "\n" + address;
    }

}
