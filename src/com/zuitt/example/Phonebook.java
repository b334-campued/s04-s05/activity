package com.zuitt.example;

import java.util.ArrayList;

public class Phonebook {
    private ArrayList<Contact> contacts;



    public Phonebook() {
        contacts = new ArrayList<>();
    }

    public void add(Contact contact) {
        this.contacts.add(contact);
    }

    public void showAll() {
        this.contacts.forEach(contact -> {
            System.out.println(contact.getName());
            System.out.println("--------------------------------------");
            System.out.println(contact.getName() + " has the following registered numbers:");
            System.out.println(contact.getContactNumber());
            System.out.println("--------------------------------------");
            System.out.println(contact.getName() + " has the following registered addressses:");
            System.out.println(contact.getAddress());
            System.out.println("======================================");
        });
    }

}
