package com.zuitt.example;

public class Main {
   public static void main(String[] args){

       Phonebook phonebook = new Phonebook();
       Contact franz = new Contact("Franz", "09123456789", "my home is in Pangasinan, Philippines");
       Contact karla = new Contact("Karla", "09987654321", "my home is in Manila, Philippines");

       franz.setContactNumber("+819271234567");
       franz.setAddress("my office is in Manila, Philippines");
       karla.setContactNumber("+639271234567");
       karla.setAddress("my office is in Quezon, Philippines");

       phonebook.add(franz);
       phonebook.add(karla);

       phonebook.showAll();


   }
}
